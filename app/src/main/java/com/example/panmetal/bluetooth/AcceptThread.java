package com.example.panmetal.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.UUID;
import java.util.logging.Handler;
import java.util.logging.LogRecord;


public class AcceptThread extends Thread {

    private final BluetoothServerSocket mmServerSocket;
    final private BluetoothAdapter BA = BluetoothAdapter.getDefaultAdapter();
    public Handler mHandler;
    public AcceptThread(){
        BluetoothServerSocket tmp = null;
        try {
            // MY_UUID is the app's UUID string, also used by the client code

            tmp = BA.listenUsingRfcommWithServiceRecord("bluetooth-pufi", UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"));
        } catch (IOException e) {
        Log.d("BLAD","BLAD1");
        }
        mmServerSocket = tmp;
    }

    public void run(){
        BluetoothSocket socket = null;

        Log.d("BLAD","probuje");
        while(true){
            try{
                socket = mmServerSocket.accept();
            }catch (IOException e){
                Log.d("BLAD","BLAD2");
                break;
            }
            if(socket != null){
                try {
                    Log.d("BLAD","POLACZYL SIE");
                    mmServerSocket.close();
                    OutputStream output = socket.getOutputStream();
                    while (true){
                     output.write("hello to ja\n\0".getBytes());
                    }

                }catch (IOException e){
                    Log.d("BLAD","BLAD3");
                    break;
                }
            }
        }
    }

}
