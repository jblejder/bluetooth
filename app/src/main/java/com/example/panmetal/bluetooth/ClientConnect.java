package com.example.panmetal.bluetooth;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

/**
 * Created by PanMetal on 2015-04-23.
 */
public class ClientConnect extends Thread {
    private BluetoothSocket mmSocket;
    public BluetoothDevice mmDevice;
    private ProgressDialog mBluetoothConnectProgressDialog;
    final private BluetoothAdapter BA = BluetoothAdapter.getDefaultAdapter();

    public ClientConnect(BluetoothDevice bd) {
        mmDevice = bd;
        Log.d("wyswietlam co szuka", bd.getName());
    }

    public void run() {
        try {
            Log.d("client", "nazwa " + mmDevice.getName() + " uuid " + mmDevice.getUuids()[0].getUuid().toString());
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(UUID.fromString("0000110a-0000-1000-8000-00805f9b34fb"));
            BA.cancelDiscovery();
            mmSocket.connect();
        } catch (IOException e) {
            Log.d("client", "CouldNotConnectToSocket", e);
            //zamknac
        }

    }
}
/*
    public void run() {
        Log.d("client","0");
        BA.cancelDiscovery();
Log.d("client","1");
        while(!mmSocket.isConnected()){
        try {
            Log.d("client","2");
                mmSocket.connect();
            Log.d("client","poloczono");
        } catch (IOException conExc) {
            Log.d("client", "blad polaczenia");
            //try {
            Log.d("client", "zamykanies");
            //   mmSocket.close();
            Log.d("client", "zamkniecie");
            //  } catch (IOException closeExc) {
            //}
        }
        }

        try {
            OutputStream OS = mmSocket.getOutputStream();
            while (true) {
                Log.d("clent","napierdziela");
                OS.write("CZESC \n".getBytes());
            }
        }catch (IOException dupa){}
    }
} */
