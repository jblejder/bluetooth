package com.example.panmetal.bluetooth;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

/*
pisze co masz zrobic bo pewni juz nie wiesz o co chodzi ;P

Pierwszy moj pomysl sprawdzenia problemy czy UUID jest tak bardzo wazne:
    Prosze Cie bys napisala funkcje przesylania juz komunikatow. Powiedzmy (dla uproszczenia)
    ze server ma odbierac dane a klient wysylas. i masz zrobic zeby klient wysylal a serwer odbieral
    a potem wyswietlal to co odebral w Log.d ( )
    Potem zainstaluj na swoim i jakims tam telefonie te apke i zobacz czy sie polaczy i czy serwer bedzie odbieral i pokazywal informacje w log.d()
     Bo moze dlatego to nie dziala bo wlasnie probojemy sie polaczyc uzywajac zly UUID

Drugi pomysl: (latwiejszy ...chyba)
    zrobic zeby jednak skanowal okolice pobieral dane z okolicy i laczyl sie uzywajac danych z nowo pobranego urzadzenia to mozesz zrobic uzywajac tego terminala ktory uzywalismy
    No bo moze jest tak ze jak mam serwer ktory dziala u nas. to tamta aplikacja robi wlasnie tak ze wyszukuje pobliskie pobiera nowe UUID i sie laczy.
    A jak ja mam na sztywno zrobione jakies UUID podane to dupa. No i jak masz metode BluetoothDevice.getUUIDs, on zwraca tablice z uuidami wiec
    bluetoothdevice.getUUID()[0]. No i wymyslilem ze skoro jest tablica mozna by sprobowac polaczyc sie uzywajac wszytkich uuid na tej tablicy jeszcze nie mialem czasu tego sprawdzic
 */


public class MainActivity extends Activity {

    final private BluetoothAdapter BA = BluetoothAdapter.getDefaultAdapter();
    private BluetoothDevice BD, BtoConnect;
    private BluetoothSocket BS;
    private ListView lv;
    ArrayList<String> list;
    ArrayList<String> listAdapter = new ArrayList<>();
    ArrayList<BluetoothDevice> listDevices = new ArrayList<>();
    Set<BluetoothDevice> pairedDevices;
    //dodane
    private BluetoothSocket mBluetoothSocket;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothDevice mBluetoothDevice;
    private UUID applicationUUID = UUID.fromString("0000110a-0000-1000-8000-00805f9b34fb");
    private ProgressDialog mBluetoothConnectProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);


        lv = (ListView) findViewById(R.id.listView1);

        findViewById(R.id.blutOn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TurnBlutOn();
            }
        });
        findViewById(R.id.blutOff).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TurnBlutOff();
            }
        });
        findViewById(R.id.butConnect).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Connect();
            }
        });
        findViewById(R.id.butSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DiscoveringDevices();
            }
        });
        findViewById(R.id.send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Send();
            }
        });

        findViewById(R.id.list).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showList();
            }
        });
    }

    //wlaczam bluetooth
    public void TurnBlutOn() {
        if (!BA.isEnabled()) {
            Log.w("Button", "TBOm");
            Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOn, 1);
            Toast.makeText(getApplicationContext(), "Turned on"
                    , Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Already on", Toast.LENGTH_SHORT).show();
        }
    }

    //wylaczanie ]
    public void TurnBlutOff() {
        Log.w("Button", "TBOff");
        BA.disable();
        Toast.makeText(getApplicationContext(), "Disabled", Toast.LENGTH_SHORT).show();
    }

    //lista sparowanych
    public void showList() {

        pairedDevices = BA.getBondedDevices();
        list = new ArrayList<>();

        lv.setClickable(true);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                BD = pairedDevices.toArray(new BluetoothDevice[1])[position];

                EditText v;
                for (int i = 0; i < lv.getCount(); i++) {
                    lv.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                }
                view.setBackgroundColor(Color.DKGRAY);
                Toast.makeText(getApplicationContext(), BD.getName() + "\nmac " + BD.getAddress(), Toast.LENGTH_SHORT).show();
            }
        });

        for (BluetoothDevice bt : pairedDevices) {
            list.add(bt.getName());
            Log.d("device", bt.getName() + " uuid: " + bt.getUuids().length + " : " + bt.getUuids()[0]);
        }
        Toast.makeText(getApplicationContext(), "Showing Paired Devices",
                Toast.LENGTH_SHORT).show();
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
        lv.setAdapter(adapter);
    }

    //uruchomienie watku serwera
    public void Connect() {
        Toast.makeText(getApplicationContext(), "Server Started", Toast.LENGTH_SHORT).show();
        Thread a = new AcceptThread();

        Log.d("cos", a.getState().toString());
        a.start();
        Log.d("cos", a.getState().toString());
    }

    //wysylanie, uruchomienie watku clienta
    public void Send() {
        //    Toast.makeText(getApplicationContext(),"Client Started",Toast.LENGTH_SHORT).show();
        Thread client = new ClientConnect(BtoConnect);
        Log.d("cos", client.getState().toString());
        client.start();

        // b.start();
        Log.d("cos", client.getState().toString());

    }

    //wyszukiwanie dostepnych urzadzen
    public void DiscoveringDevices() {

        lv = (ListView) findViewById(R.id.listView1);
        BA.startDiscovery();
        list = new ArrayList<>();
        lv.setClickable(true);

        final BroadcastReceiver mReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Log.w("tag", "0");
                String action = intent.getAction();
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    //listDevices.add(device.getName());//+ "\n" + device.getAddress());
                    //Log.d("znalazl",device.getName());
                    listDevices.add(device);
                }
            }
        };
        //register
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter);

        Toast.makeText(getApplicationContext(), "Showing Available Devices",
                Toast.LENGTH_SHORT).show();

//      Log.d("i",Integer.toString(listDevices.size()));
        //z listy devicow zrobic liste stringow

        for (BluetoothDevice temp : listDevices) {
            listAdapter.add(temp.getName());
            //  Log.d("znalazl",temp.getName());
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, listAdapter);
        lv.setAdapter(adapter);
        Log.w("tag", "1");
        //klikniecie i wyswietlenie kliknietego
        final BluetoothDevice[] cos = listDevices.toArray(new BluetoothDevice[listDevices.size()]);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                BtoConnect = cos[position];
                TextView clickedView = (TextView) view;
                Toast.makeText(MainActivity.this, clickedView.getText(), Toast.LENGTH_SHORT).show();
                //  Toast.makeText(getApplicationContext(),BtoConnect.getName(),Toast.LENGTH_SHORT);
                Log.d("tujestem", BtoConnect.getName());
            }
        });
    }
}
